@extends('layouts.client')

@section('content')
    <div class="container">
        <br>
        <div class="row">
            <div class="col">
                <p>
                <h3><b>Profile</b> {{$user->name}}</h3>
                </p>
            </div>
        </div>
        <br>
        <form action="{{action([App\Http\Controllers\PhotosController::class, 'index'])}}">
            <button class="btn btn-primary">Назад</button>
        </form>

        <br>
        <b>My photos {{$user->name}}</b>
        <form action="{{action([App\Http\Controllers\PhotosController::class, 'create'])}}">
            <button class="btn btn-primary">Create new photo</button>
        </form>
        <br>
        <br>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Photo</th>
                    <th scope="col">Title</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($user->photos as $photo)
                    <tr>
                        <td>{{$photo->id}}</td>
                        <td>
                                <img width="100" height="100" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
                        </td>
                        <td>
                            <a href="{{route('client.photos.show', ['photo' => $photo])}}">
                                {{$photo->title}}
                            </a>
                        </td>
                        <td>
                            <form action="{{action([App\Http\Controllers\PhotosController::class, 'destroy'],
                                        ['photo'=> $photo])}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
{{--        <div class="row">--}}
{{--            @foreach($user->photos as $photo)--}}
{{--                <div class="card" style="width: 18rem;">--}}
{{--                    <img class="card-img-top" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$book->picture}}">--}}
{{--                    <div class="card-body">--}}
{{--                        <h5 class="card-title"><a href="{{route('client.books.show', ['book' => $book])}}">{{$book->title}}</a></h5>--}}
{{--                        <p class="card-text">--}}
{{--                            @if(!is_null($book->author))--}}
{{--                                Автор: <a href="{{route('client.authors.show', ['author' => $book->author])}}">{{$book->author->name}}</a>--}}
{{--                            @endif--}}
{{--                            <br>--}}
{{--                            @if(!is_null($book->genre))--}}
{{--                                Жанр: <a href="{{route('client.genres.show', ['genre' => $book->genre])}}">{{$book->genre->name}}</a>--}}
{{--                            @endif--}}
{{--                        </p>--}}
{{--                        <footer class="blockquote-footer">--}}
{{--                            Цена: {{number_format($book->price, 2)}}--}}
{{--                        </footer>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                --}}{{--                @endif--}}
{{--            @endforeach--}}
{{--        </div>--}}
    </div>

@endsection
