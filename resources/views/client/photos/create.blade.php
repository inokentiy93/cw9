@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col">
                <br>
                <h5>Create new photo</h5>
            </div>
            <div class="col"></div>
        </div>
        <div class="row">
            <div class="col">
                <form method="post" action="{{ route('client.photos.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="title">Caption:</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}"/>
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="mb-3">
                            <label class="form-control-file" for="custom-file">Image:</label>
                            <input type="file" class="form-control-file" name="picture" id="custom-file"/>
                        </div>
                        @error('picture')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button class="btn btn-primary" type="submit">Create photo</button>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection


