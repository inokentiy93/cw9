@extends('layouts.client')

@section('content')
    <div class="container">
        <br>
        <div class="row">
            <div class="col">
                <p>
                    <h3><b>{{$photo->title}}</b></h3>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p>
                        <img width="500" height="500" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p>
                    <b>"@lang('Photo By:') </b> {{$photo->user->name}}, {{$photo->created_at->diffForHumans()}}
                </p>
            </div>
        </div>
        <br>
        <form action="{{action([App\Http\Controllers\PhotosController::class, 'index'])}}">
            <button class="btn btn-primary">Назад</button>
        </form>
        <br>
        <br>
        <h3>@lang('Comments')</h3>
        @foreach($photo->comments as $comment)
            <p>@lang('By:') {{$comment->user->name}}, @lang('Score:') {{$comment->grade}}</p>
            <p>{{$comment->body}}</p>
            <hr>
        @endforeach
        @can('create', $comment)
        <div class="row">
            <div class="col">
                <form method="post" action="{{ route('client.photos.comments.store', ['photo' => $photo])}}">
                    @csrf
                    <input type="hidden" id="book_id" value="{{$photo->id}}">
                    <div class="mb-3">
                        <label for="body">Text:</label>
                        <input type="text" class="form-control" name="body" id="body" value="{{ old('body') }}"/>
                        @error('body')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="grade">Score</label>
                            <select name="grade" class="form-control" id="grade">
                                <option value=1>1</option>
                                <option value=2>2</option>
                                <option value=3>3</option>
                                <option value=4>4</option>
                                <option value=5>5</option>
                            </select>
                        @error('grade')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button class="btn btn-primary" type="submit">Create comment</button>
                    <br>
                </form>
            </div>
    @endcan
@endsection
