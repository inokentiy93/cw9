@extends('layouts.client')

@section('content')
    <div class="container">
        <br>
        <div class="row">
            @foreach($photos as $photo)
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{asset('/storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{route('client.photos.show', ['photo' => $photo])}}">{{$photo->title}}</a></h5>
                        <h5 class="card-title">By {{$photo->user->name}}</h5>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row justify-content-md-center p-5">

        <div class="col-md-auto">

            {{ $photos->links('pagination::bootstrap-4') }}

        </div>

    </div>

@endsection
