$(document).ready(function () {

    $('#create-comment-btn').on('click', function (e) {
        e.preventDefault();

        const photoId = $('#photo_id').val();

        $.ajax({
            method: 'POST',
            url: `/client/photos/${photoId}/comments`,
            data: $('#create-comment').serialize(),
        }).done(response => {
            console.log(response);
            renderComment(response.comment);
        }).fail(function(xhr, textStatus, errorThrown) {
            alert(xhr.responseText);
        });
    });

    const renderComment = comment => {
        const commentsBlock = $('.scrollit');
        $(commentsBlock).prepend(comment);
        resetForm();
    };

    const resetForm = () => {
        $('#create-comment').trigger('reset');
    }

});
