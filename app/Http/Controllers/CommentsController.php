<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{


    /**
     * @param Request $request
     * @param Photo $photo
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Request $request, Photo $photo): RedirectResponse
    {
        $this->authorize('create', Comment::class);
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->grade = $request->input('grade');
        $comment->photo_id = $photo->id;;
        $comment->user()->associate($request->user());
        $comment->save();
        return back();
    }
}
