<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class PhotosController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::latest()->paginate(6);
        return view('client.photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Photo::class);
        return view('client.photos.create');
    }


    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('update', Photo::class);
        $data = $request->all();
        $file = $request->file('picture');
        $path = $file->store('photos_pictures', 'public');
        $data['picture'] = $path;

        $photo = new Photo($data);
        $photo->user()->associate($request->user());
        $photo->save();
        return redirect(route('client.photos.index'))->with('success', "$photo->title added successfully");
    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {
        return view('client.photos.show', compact('photo'));
    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Models\Photo  $photo
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(Photo $photo)
//    {
//        return view('client.photos.edit', compact('photo'));
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \App\Models\Photo  $photo
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, Photo $photo)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Photo $photo
     * @return Application|Redirector|RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Photo $photo)
    {
        $this->authorize('delete', Photo::class);
        $photo->delete();
        return redirect(route('client.photos.index'))->with('success', "$photo->title deleted successfully");
    }
}
