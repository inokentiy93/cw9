<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [App\Http\Controllers\PhotosController::class, 'index'])->name('index')->middleware('language');

Route::prefix('client')->middleware(['auth', 'language'])->name('client.')->group(function () {
    Route::resource('photos', \App\Http\Controllers\PhotosController::class)->only(['index', 'show', 'create', 'store', 'destroy']);
    Route::resource('users', \App\Http\Controllers\UsersController::class)->only(['show']);
    Route::resource('photos.comments', \App\Http\Controllers\CommentsController::class)->only(['store']);
//    Route::resource('authors', AuthorsController::class)->only(['index', 'show']);
//    Route::resource('books.comments', CommentsController::class)->only(['store']);
//    Route::resource('comments', CommentsController::class)->only(['edit', 'update', 'destroy']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('language/{locale}', [\App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')->where('locale', 'en|ru');
