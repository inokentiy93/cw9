<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->paragraph(),
            'grade' => $this->faker->numberBetween(1, 5),
            'photo_id' => rand(1, 20),
            'user_id' => rand(1, 3)
        ];
    }
}
