<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Photo: ' . $this->faker->word(),
            'picture' => $this->getImage(rand(1, 3)),
            'user_id' => rand(1, 3)
        ];
    }

    private function getImage(int $number = 1): string
    {
        $path = storage_path() . "/for_seed_photo/" . $number . ".png";
        $image_name = md5($path) . ".png";
        $resize = Image::make($path)->fit(300)->encode('png');
        Storage::disk('public')->put('photos_pictures/'.$image_name, $resize->__toString());
        return 'photos_pictures/'.$image_name;
    }
}
